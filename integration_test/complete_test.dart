import 'package:flutter_driver/flutter_driver.dart' as d;
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  group("Complete group", () {
    final openButton = d.find.byValueKey("OpenButton");
    final gridButton = d.find.byValueKey("GridViewButton");
    final listButton = d.find.byValueKey("ListViewButton");
    final galleryContent = d.find.byValueKey("GalleryContent");
    final successWidget = d.find.byValueKey("SuccessWidget");
    d.FlutterDriver driver;

    Future<bool> isPresent(d.SerializableFinder byValueKey, {Duration timeout = const Duration(seconds: 1)}) async {
      try {
        await driver.waitFor(byValueKey, timeout: timeout);
        return true;
      } catch (exception) {
        return false;
      }
    }

    setUpAll(() async {
      driver = await d.FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });
    test("Open and go to gallery", () async {
      if (await isPresent(openButton)) {
        await driver.tap(openButton);
      }
    });
    test("Switch list to grid", () async {
      if (await isPresent(gridButton)) {
        await driver.tap(gridButton);
      }
    });
    test("Switch grid to list", () async {
      if (await isPresent(gridButton)) {
        await driver.tap(listButton);
      }
    });
    test("Scroll list view", () async {
      if (await isPresent(galleryContent)) {
        await driver.scroll(galleryContent, 200, 0, new Duration(seconds: 5));
      }
    });
    test("Scroll list view", () async {
      if (await isPresent(successWidget)) {
        await driver.tap(successWidget);
      }
    });
  });
}
