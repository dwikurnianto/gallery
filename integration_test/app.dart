import 'package:integration_test/integration_test.dart';
import 'package:gallery/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  app.main();
}
