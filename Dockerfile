# This Dockerfile creates a static build image for CI

FROM openjdk:8-jdk

# Just matched `app/build.gradle`
ENV ANDROID_COMPILE_SDK "28"
# Just matched `app/build.gradle`
ENV ANDROID_BUILD_TOOLS "29.0.1"
# Version from https://developer.android.com/studio/releases/sdk-tools
ENV ANDROID_SDK_TOOLS "4333796"
# Flutter sdk
# ENV FLUTTER_VERSION "https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_v1.7.8+hotfix.4-stable.tar.xz"
ENV FLUTTER_VERSION "https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_2.0.2-stable.tar.xz"

ENV ANDROID_HOME /android-sdk-linux
ENV PATH="${PATH}:/android-sdk-linux/platform-tools/"

# install OS packages
RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1 build-essential ruby ruby-dev
# We use this for xxd hex->binary
RUN apt-get --quiet install --yes vim-common
# install Android SDK
RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
RUN unzip -d android-sdk-linux android-sdk.zip
RUN echo y | echo y | android-sdk-linux/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
RUN echo y | echo y | android-sdk-linux/tools/bin/sdkmanager "platform-tools" >/dev/null
RUN echo y | echo y | android-sdk-linux/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null

ENV PATH="$ANDROID_HOME/emulator:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools:$DART_SDK_HOME/bin:$FLUTTER_HOME/bin:$PATH"

RUN echo "Accepting licenses"; yes | sdkmanager --licenses

# flutter sdk setup
RUN echo y | wget --output-document=flutter-sdk.tar.xz $FLUTTER_VERSION
RUN echo y | tar -xf flutter-sdk.tar.xz
ENV PATH="${PATH}:$PWD/flutter/bin"

# Disable flutter analytics
RUN flutter config --no-analytics

# install FastLane
RUN gem install bundle

# Initial fastlane installation
COPY android/Gemfile beeazubi-app/android/
RUN cd beeazubi-app/android && bundle install

# last step so we can use the full caching
COPY . beeazubi-app
