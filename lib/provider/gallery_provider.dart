import 'package:flutter/material.dart';
import 'package:gallery/model/photos/GetPhotosRequest.dart';
import 'package:gallery/model/photos/GetPhotosResponse.dart';

class GalleryProvider extends ChangeNotifier {
  List<Photos> _photos;
  GetPhotosRequest _request;

  List<Photos> get getPhotos => _photos;
  GetPhotosRequest get getPhotosRequest => _request;

  void changePhotos(List<Photos> photos) {
    _photos = photos;
    notifyListeners();
  }

  void addPhotos(List<Photos> photos) {
    _photos.addAll(photos);
    notifyListeners();
  }

  void changeRequest(GetPhotosRequest request) {
    _request = request;
    notifyListeners();
  }
}
