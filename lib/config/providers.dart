import 'package:gallery/provider/gallery_provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

List<SingleChildWidget> providers() {
  return [
    ChangeNotifierProvider(create: (context) => GalleryProvider()),
  ];
}
