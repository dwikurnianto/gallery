import 'package:flutter/material.dart';
import 'package:gallery/persentation/screen/about.dart';
import 'package:gallery/persentation/screen/detail.dart';
import 'package:gallery/persentation/screen/home.dart';

/// Generated route, (pass data to named route)
/// Template by @Dwi Kurnianto Mulyadien
/// dwikurnianto.mulyadien@gmail.com
///
MaterialPageRoute<dynamic> generatedRoutes(settings) {
  MaterialPageRoute routes;
  switch (settings.name) {
    case Detail.routeName:
      var args = settings.arguments;
      routes = MaterialPageRoute(
        builder: (context) {
          return Detail(args);
        },
      );
      break;
  }
  return routes;
}

/// Dictionary of named route
/// This is only for named route without data / argument to pass

Map<String, WidgetBuilder> routes() {
  return <String, WidgetBuilder>{
    About.routeName: (BuildContext context) => new About(),
    Home.routeName: (BuildContext context) => new Home(),
  };
}
