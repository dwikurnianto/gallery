import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData themes(BuildContext context) {
  return ThemeData(
    brightness: Brightness.light,
    backgroundColor: Colors.grey[50],
    canvasColor: Colors.grey[50],
    primaryColor: Colors.blue,
    primaryTextTheme: TextTheme(bodyText1: TextStyle(color: Colors.grey[900], fontSize: 14), bodyText2: TextStyle(color: Colors.grey, fontSize: 12), button: TextStyle(color: Colors.white)),
    primaryIconTheme: IconThemeData(color: Colors.blue),
    accentColor: Colors.blue[900],
    colorScheme: ColorScheme(
      primary: Colors.blue,
      primaryVariant: Colors.blue[900],
      secondary: Colors.yellow,
      secondaryVariant: Colors.yellow[900],
      surface: Colors.white,
      background: Colors.white,
      error: Colors.red,
      onPrimary: Colors.white,
      onSecondary: Colors.black,
      onSurface: Colors.grey[900],
      onBackground: Colors.grey[900],
      onError: Colors.white,
      brightness: Brightness.light,
    ),
    primarySwatch: Theme.of(context).colorScheme.primary,
    buttonTheme: ButtonThemeData(
      disabledColor: Colors.blue[100],
      colorScheme: Theme.of(context).colorScheme,
      buttonColor: Theme.of(context).colorScheme.primary,
      minWidth: double.maxFinite,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
    ),
    buttonColor: Theme.of(context).colorScheme.primary,
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.pressed)) return Theme.of(context).colorScheme.primary.withOpacity(0.5);
            return Theme.of(context).colorScheme.primary;
          },
        ),
        foregroundColor: MaterialStateProperty.all(
          Theme.of(context).colorScheme.onPrimary,
        ),
        minimumSize: MaterialStateProperty.all(
          Size(120, 38),
        ),
        textStyle: MaterialStateProperty.all(
          TextStyle(color: Colors.white),
        ),
        elevation: MaterialStateProperty.all(0),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      isDense: true,
      alignLabelWithHint: false,
      focusColor: Theme.of(context).colorScheme.primary,
      fillColor: Colors.grey[100],
      filled: true,
      enabledBorder: InputBorder.none,
    ),
    appBarTheme: AppBarTheme(
      brightness: Brightness.light,
      textTheme: TextTheme(
        caption: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
        headline6: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
      ),
      elevation: 0,
      iconTheme: IconThemeData(
        color: Theme.of(context).colorScheme.onPrimary,
      ),
      foregroundColor: Theme.of(context).colorScheme.onPrimary,
    ),
    textTheme: GoogleFonts.openSansTextTheme(
      TextTheme(
        bodyText1: TextStyle(color: Theme.of(context).colorScheme.onSurface, fontSize: 14, fontWeight: FontWeight.bold),
        bodyText2: TextStyle(color: Theme.of(context).colorScheme.onSurface, fontSize: 14),
        button: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
      ),
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Theme.of(context).colorScheme.primary,
      foregroundColor: Theme.of(context).colorScheme.onPrimary,
    ),
  ).copyWith(
    pageTransitionsTheme: const PageTransitionsTheme(
      builders: <TargetPlatform, PageTransitionsBuilder>{
        TargetPlatform.android: OpenUpwardsPageTransitionsBuilder(),
      },
    ),
  );
}
