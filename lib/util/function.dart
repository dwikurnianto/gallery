String getQueryString(Map params, {String prefix: '&', bool inRecursion: false}) {
  String query = '';
  int index = 0;
  params.forEach((key, value) {
    (index == 0) ? prefix = "?" : prefix = "&";
    if (inRecursion) key = '[$key]';
    if (value is String || value is int || value is double || value is bool) {
      query += '$prefix$key=$value';
    } else if (value is List || value is Map) {
      if (value is List) value = value.asMap();
      value.forEach((k, v) {
        (k == 0) ? prefix = "" : prefix = "&";
        query += getQueryString({k: v}, prefix: '$prefix$key', inRecursion: true);
      });
    }
    index++;
  });
  return query;
}
