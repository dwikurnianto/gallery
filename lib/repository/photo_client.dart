import 'dart:async';
import 'dart:convert';

import 'package:gallery/base/client_base.dart';
import 'package:gallery/model/photos/GetPhotosRequest.dart';
import 'package:gallery/model/photos/GetPhotosResponse.dart';
import 'package:gallery/util/function.dart';
import 'package:http/http.dart' show get;
import 'dart:developer' as developer;

import '../env.dart';

class PhotoClient extends ClientBase {
  Future<GetPhotosResponse> getPhotos(GetPhotosRequest _request) async {
    String url = "${Env.baseApiPexels}/search";
    url = "$url${getQueryString(_request.toJson())}";
    developer.log(url, name: "Request");
    return get(url, headers: header()).timeout(const Duration(seconds: 10)).then((response) {
      developer.log(response.body, name: "Response");
      return GetPhotosResponse.fromJson(json.decode(response.body));
    }).catchError((ex) {
      developer.log(ex.toString(), name: "Exception");
    });
  }
}
