import 'package:flutter/material.dart';
import 'package:gallery/model/photos/GetPhotosRequest.dart';
import 'package:gallery/model/photos/GetPhotosResponse.dart';
import 'package:gallery/persentation/component/http_image_loader.dart';
import 'package:gallery/persentation/component/waiter.dart';
import 'package:gallery/persentation/screen/detail.dart';
import 'package:gallery/provider/gallery_provider.dart';
import 'package:gallery/repository/photo_client.dart';
import 'package:provider/provider.dart';

class ListViewGallery extends StatefulWidget {
  @override
  _ListViewGalleryState createState() => _ListViewGalleryState();
}

class _ListViewGalleryState extends State<ListViewGallery> {
  PhotoClient _photoClient = new PhotoClient();
  ConnectionState _connectionState = ConnectionState.none;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        NotificationListener(
          onNotification: onNotification,
          child: Consumer<GalleryProvider>(
            builder: (context, provider, child) {
              return ListView.separated(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                padding: EdgeInsets.all(15),
                itemCount: provider?.getPhotos?.length ?? 0,
                separatorBuilder: (context, index) {
                  return SizedBox(height: 10);
                },
                itemBuilder: (context, index) {
                  return itemTile(context, provider?.getPhotos[index]);
                },
              );
            },
          ),
        ),
        Waiter(_connectionState == ConnectionState.waiting),
      ],
    );
  }

  Widget itemTile(BuildContext context, Photos photos) {
    return Material(
      borderRadius: BorderRadius.circular(15),
      color: Colors.white,
      child: InkWell(
        borderRadius: BorderRadius.circular(15),
        onTap: () {
          Navigator.pushNamed(context, Detail.routeName, arguments: photos);
        },
        child: Container(
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            children: [
              HttpImageLoader(
                imageUrl: photos.src.medium,
                height: 100,
                width: 100,
                fit: BoxFit.cover,
                borderRadius: BorderRadius.circular(10),
              ),
              SizedBox(width: 15),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      photos.photographer,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(
                      photos.photographerUrl,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _fetchMorePhoto() {
    GalleryProvider _provider = Provider.of<GalleryProvider>(context, listen: false);
    setState(() => _connectionState = ConnectionState.waiting);
    GetPhotosRequest _tempGetPhotoRequest = _generateRequest();
    _photoClient.getPhotos(_tempGetPhotoRequest).then((_response) {
      if (_response?.photos != null) {
        _provider.addPhotos(_response.photos);
        _provider.changeRequest(_tempGetPhotoRequest);
      }
    }).whenComplete(() {
      setState(() => _connectionState = ConnectionState.done);
    });
  }

  bool onNotification(ScrollNotification notification) {
    if (notification is ScrollUpdateNotification) {
      if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
        print(notification.metrics.maxScrollExtent);
        if (_connectionState != null && _connectionState != ConnectionState.waiting) _fetchMorePhoto();
      }
    }
    return true;
  }

  GetPhotosRequest _generateRequest() {
    GalleryProvider _provider = Provider.of<GalleryProvider>(context, listen: false);
    GetPhotosRequest _getPhotosRequest = _provider.getPhotosRequest;
    return GetPhotosRequest(
      query: _getPhotosRequest.query,
      page: ((_getPhotosRequest != null) ? (_getPhotosRequest.page + 1) : _getPhotosRequest.page),
      perPage: ((_getPhotosRequest != null) ? _getPhotosRequest.perPage : _getPhotosRequest.perPage),
    );
  }
}
