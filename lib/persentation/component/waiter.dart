import 'package:flutter/material.dart';

class Waiter extends StatelessWidget {
  final bool isVisible;
  Waiter(this.isVisible);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: isVisible,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
        margin: EdgeInsets.all(25),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 3.0,
              offset: const Offset(0.0, 1.0),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("Mohon tunggu"),
            SizedBox(width: 10),
            CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }
}
