import 'package:flutter/material.dart';

class ErrorIllustration extends StatelessWidget {
  final Function callback;
  ErrorIllustration({
    Key key,
    this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.asset(
              "assets/no_internet.png",
              height: 150,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 15),
          Text(
            "Ups terjadi kesalahan",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          Text(
            "Sepertinya tidak ada konektifitas internet",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText2,
          ),
          SizedBox(height: 10),
          TextButton(
            onPressed: () {
              callback();
            },
            child: Text("Muat ulang"),
          )
        ],
      ),
    );
  }

  // Widget _noConnectionError(BuildContext context) {}
}
