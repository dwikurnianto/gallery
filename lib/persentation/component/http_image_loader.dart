import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gallery/persentation/component/shimmer_box.dart';

class HttpImageLoader extends StatelessWidget {
  final String imageUrl;
  final double height;
  final double width;
  final BoxFit fit;
  final BorderRadiusGeometry borderRadius;
  HttpImageLoader({this.imageUrl, this.height, this.width, this.fit, this.borderRadius});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: borderRadius ?? BorderRadius.zero,
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        height: height,
        width: width,
        fit: fit,
        placeholder: (context, url) {
          return ShimmerBox(
            height: height,
            width: width,
            borderRadius: borderRadius,
          );
        },
        errorWidget: (context, url, error) => Container(
          color: Colors.grey[300],
          height: height,
          width: width,
          child: Center(
            child: Text(
              "No image",
              style: Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.grey),
            ),
          ),
        ),
      ),
    );
  }
}
