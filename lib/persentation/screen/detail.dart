import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gallery/model/photos/GetPhotosResponse.dart';
import 'package:gallery/persentation/component/http_image_loader.dart';

class Detail extends StatefulWidget {
  static const String routeName = "detail";
  final Photos photos;
  Detail(this.photos);
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  int color;
  @override
  void initState() {
    super.initState();
    color = int.tryParse(("0xff${widget.photos.avgColor}").replaceAll('#', ''));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark, // status bar brightness
        title: Text(widget.photos.photographer),
        elevation: 0,
        backgroundColor: Colors.transparent,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: <Color>[
                Colors.black45,
                Colors.transparent,
              ],
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Hero(
            tag: widget.photos.src.medium,
            child: HttpImageLoader(
              imageUrl: widget.photos.src.large,
              height: 400,
              width: double.maxFinite,
              fit: BoxFit.cover,
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.photos.photographer,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  Text(
                    widget.photos.photographerUrl,
                    maxLines: 2,
                    style: Theme.of(context).textTheme.bodyText2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
