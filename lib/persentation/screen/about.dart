import 'package:flutter/material.dart';
import 'package:gallery/persentation/screen/home.dart';
import 'package:url_launcher/url_launcher.dart';

class About extends StatefulWidget {
  static const String routeName = "about";
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.maxFinite,
          height: double.maxFinite,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset(
                  "assets/author.jpg",
                  height: 200,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: 15),
              Text(
                "Dwi Kurnianto Mulyadien",
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Text(
                "dwikurnianto.mulyadien@gmail.com",
                style: Theme.of(context).textTheme.bodyText2,
              ),
              SizedBox(height: 15),
              TextButton(
                key: Key("LaunchButton"),
                onPressed: () {
                  _goToGitlab();
                },
                child: Text(
                  "Gitlab repository",
                ),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith<Color>(
                    (Set<MaterialState> states) {
                      if (states.contains(MaterialState.pressed)) return Theme.of(context).colorScheme.secondaryVariant.withOpacity(0.5);
                      return Theme.of(context).colorScheme.secondaryVariant;
                    },
                  ),
                  foregroundColor: MaterialStateProperty.all(
                    Theme.of(context).colorScheme.onPrimary,
                  ),
                  minimumSize: MaterialStateProperty.all(Size(175, 38)),
                ),
              ),
              TextButton(
                key: Key("OpenButton"),
                onPressed: () {
                  Navigator.pushNamed(context, Home.routeName);
                },
                child: Text(
                  "Oke, buka aplikasi",
                ),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith<Color>(
                    (Set<MaterialState> states) {
                      if (states.contains(MaterialState.pressed)) return Theme.of(context).colorScheme.primary.withOpacity(0.5);
                      return Theme.of(context).colorScheme.primary;
                    },
                  ),
                  foregroundColor: MaterialStateProperty.all(Theme.of(context).colorScheme.onPrimary),
                  minimumSize: MaterialStateProperty.all(Size(175, 38)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _goToGitlab() async => await launch("https://gitlab.com/dwikurnianto/gallery.git");
}
