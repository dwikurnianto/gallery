import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:gallery/model/photos/GetPhotosRequest.dart';
import 'package:gallery/model/photos/GetPhotosResponse.dart';
import 'package:gallery/persentation/component/error_illustration.dart';
import 'package:gallery/persentation/component/grid_view_gallery.dart';
import 'package:gallery/persentation/component/http_image_loader.dart';
import 'package:gallery/persentation/component/list_view_gallery.dart';
import 'package:gallery/provider/gallery_provider.dart';
import 'package:gallery/repository/photo_client.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  static const String routeName = "home";
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  /// PhotoClient is a async script which allow us to fetch data from end point
  PhotoClient _photoClient = new PhotoClient();

  /// On the other side, [_getPhotosResponse] is a future which held data from previous class result [_photoClient].
  Future<GetPhotosResponse> _getPhotosResponse;
  GetPhotosRequest _getPhotoRequest;
  LayoutMode _layoutMode;

  @override
  void initState() {
    super.initState();

    /// This function will allow the program to wait until it has a [context]
    /// then, it will trigger the [_fetchPhoto] method with context
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _fetchPhoto(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key("HomeScaffold"),
      body: RefreshIndicator(
        onRefresh: _onRefresh,
        displacement: 70,
        child: NestedScrollView(
          key: Key("GalleryContent"),
          physics: BouncingScrollPhysics(),
          body: FutureBuilder<GetPhotosResponse>(
            future: _getPhotosResponse,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting)
                return Center(
                  key: Key("LoaderWidget"),
                  child: CircularProgressIndicator(),
                );
              if (snapshot.hasData) {
                return Visibility(
                  key: Key("SuccessWidget"),
                  visible: _layoutMode == LayoutMode.listView,
                  replacement: GridViewGallery(),
                  child: ListViewGallery(),
                );
              }
              if (snapshot.hasError) {
                return ErrorIllustration(
                  key: Key("ErrorWidget"),
                  callback: _onRefresh,
                );
              }
              return ErrorIllustration(
                key: Key("NoDataWidget"),
                callback: _onRefresh,
              );
            },
          ),
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                floating: true,
                title: Text(
                  'Awesome App',
                  style: Theme.of(context).textTheme.headline6.copyWith(
                        color: Theme.of(context).colorScheme.onSurface,
                      ),
                ),
                foregroundColor: Theme.of(context).colorScheme.onSurface,
                iconTheme: IconThemeData(
                  color: Theme.of(context).colorScheme.onSurface,
                ),
                backgroundColor: Colors.white,
                expandedHeight: 200.0,
                pinned: true,
                elevation: 0,
                actions: [
                  IconButton(
                    key: Key("GridViewButton"),
                    icon: Icon(Icons.grid_view),
                    onPressed: () {
                      setState(() {
                        _layoutMode = LayoutMode.gridView;
                      });
                    },
                    color: Theme.of(context).colorScheme.onSurface,
                  ),
                  IconButton(
                    key: Key("ListViewButton"),
                    icon: Icon(Icons.list),
                    onPressed: () {
                      setState(() {
                        _layoutMode = LayoutMode.listView;
                      });
                    },
                    color: Theme.of(context).colorScheme.onSurface,
                  ),
                ],
                flexibleSpace: FlexibleSpaceBar(
                  background: HttpImageLoader(
                    imageUrl: "https://wallpaperaccess.com/full/686998.jpg",
                    fit: BoxFit.cover,
                    height: double.maxFinite,
                    width: double.maxFinite,
                  ),
                ),
              ),
            ];
          },
        ),
      ),
    );
  }

  void _fetchPhoto(BuildContext context) {
    GalleryProvider _provider = Provider.of<GalleryProvider>(context, listen: false);
    setState(() {
      _getPhotoRequest = new GetPhotosRequest(query: "forrest", page: 1, perPage: 10);
      _getPhotosResponse = _photoClient.getPhotos(_getPhotoRequest).then((_response) {
        if (_response?.photos != null) {
          _provider.changePhotos(_response.photos);
          _provider.changeRequest(_getPhotoRequest);
        }
        return _response;
      });
    });
  }

  Future<void> _onRefresh() async {
    _fetchPhoto(context);
  }
}

enum LayoutMode { listView, gridView }
