import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gallery/config/providers.dart';
import 'package:gallery/config/routes.dart';
import 'package:gallery/config/themes.dart';
import 'package:gallery/persentation/screen/about.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Awesome App',
        routes: routes(),
        onGenerateRoute: generatedRoutes,
        theme: themes(context),
        home: About(),
      ),
    );
  }
}
