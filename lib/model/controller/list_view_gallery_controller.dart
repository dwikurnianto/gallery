import 'package:gallery/model/photos/GetPhotosResponse.dart';

class ListViewGalleryController {
  AddMore addMore;

  void dispose() {
    addMore = null;
  }
}

typedef AddMore(List<Photos> photos);
