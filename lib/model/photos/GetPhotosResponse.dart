class GetPhotosResponse {
  int page;
  int perPage;
  List<Photos> photos;
  int totalResults;
  String nextPage;

  GetPhotosResponse({this.page, this.perPage, this.photos, this.totalResults, this.nextPage});

  GetPhotosResponse.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    perPage = json['per_page'];
    if (json['photos'] != null) {
      photos = [];
      json['photos'].forEach((v) {
        photos.add(new Photos.fromJson(v));
      });
    }
    totalResults = json['total_results'];
    nextPage = json['next_page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['per_page'] = this.perPage;
    if (this.photos != null) {
      data['photos'] = this.photos.map((v) => v.toJson()).toList();
    }
    data['total_results'] = this.totalResults;
    data['next_page'] = this.nextPage;
    return data;
  }

  static const Map<String, dynamic> exampleResponse = {
    "page": 1,
    "per_page": 10,
    "photos": [
      {
        "id": 3805708,
        "width": 1721,
        "height": 2526,
        "url": "https://www.pexels.com/photo/snow-wood-road-landscape-3805708/",
        "photographer": "@rrinna",
        "photographer_url": "https://www.pexels.com/@rrrinna",
        "photographer_id": 824302,
        "avg_color": "#4A5E63",
        "src": {
          "original": "https://images.pexels.com/photos/3805708/pexels-photo-3805708.jpeg",
          "large2x": "https://images.pexels.com/photos/3805708/pexels-photo-3805708.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/3805708/pexels-photo-3805708.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/3805708/pexels-photo-3805708.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/3805708/pexels-photo-3805708.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/3805708/pexels-photo-3805708.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/3805708/pexels-photo-3805708.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/3805708/pexels-photo-3805708.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      },
      {
        "id": 6036196,
        "width": 3944,
        "height": 4930,
        "url": "https://www.pexels.com/photo/person-in-orange-jacket-standing-on-brown-wooden-house-in-the-middle-of-the-forest-6036196/",
        "photographer": "Oskar  Smethurst",
        "photographer_url": "https://www.pexels.com/@smetovisuals",
        "photographer_id": 8330698,
        "avg_color": "#1A2023",
        "src": {
          "original": "https://images.pexels.com/photos/6036196/pexels-photo-6036196.jpeg",
          "large2x": "https://images.pexels.com/photos/6036196/pexels-photo-6036196.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/6036196/pexels-photo-6036196.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/6036196/pexels-photo-6036196.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/6036196/pexels-photo-6036196.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/6036196/pexels-photo-6036196.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/6036196/pexels-photo-6036196.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/6036196/pexels-photo-6036196.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      },
      {
        "id": 6036197,
        "width": 3813,
        "height": 4766,
        "url": "https://www.pexels.com/photo/man-in-black-jacket-walking-on-pathway-between-trees-6036197/",
        "photographer": "Oskar  Smethurst",
        "photographer_url": "https://www.pexels.com/@smetovisuals",
        "photographer_id": 8330698,
        "avg_color": "#787F82",
        "src": {
          "original": "https://images.pexels.com/photos/6036197/pexels-photo-6036197.jpeg",
          "large2x": "https://images.pexels.com/photos/6036197/pexels-photo-6036197.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/6036197/pexels-photo-6036197.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/6036197/pexels-photo-6036197.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/6036197/pexels-photo-6036197.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/6036197/pexels-photo-6036197.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/6036197/pexels-photo-6036197.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/6036197/pexels-photo-6036197.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      },
      {
        "id": 6036199,
        "width": 3945,
        "height": 5917,
        "url": "https://www.pexels.com/photo/snow-wood-landscape-weather-6036199/",
        "photographer": "Oskar  Smethurst",
        "photographer_url": "https://www.pexels.com/@smetovisuals",
        "photographer_id": 8330698,
        "avg_color": "#535351",
        "src": {
          "original": "https://images.pexels.com/photos/6036199/pexels-photo-6036199.jpeg",
          "large2x": "https://images.pexels.com/photos/6036199/pexels-photo-6036199.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/6036199/pexels-photo-6036199.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/6036199/pexels-photo-6036199.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/6036199/pexels-photo-6036199.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/6036199/pexels-photo-6036199.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/6036199/pexels-photo-6036199.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/6036199/pexels-photo-6036199.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      },
      {
        "id": 6036201,
        "width": 4000,
        "height": 5000,
        "url": "https://www.pexels.com/photo/wood-landscape-water-forest-6036201/",
        "photographer": "Oskar  Smethurst",
        "photographer_url": "https://www.pexels.com/@smetovisuals",
        "photographer_id": 8330698,
        "avg_color": "#2D312D",
        "src": {
          "original": "https://images.pexels.com/photos/6036201/pexels-photo-6036201.jpeg",
          "large2x": "https://images.pexels.com/photos/6036201/pexels-photo-6036201.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/6036201/pexels-photo-6036201.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/6036201/pexels-photo-6036201.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/6036201/pexels-photo-6036201.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/6036201/pexels-photo-6036201.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/6036201/pexels-photo-6036201.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/6036201/pexels-photo-6036201.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      },
      {
        "id": 3805681,
        "width": 5306,
        "height": 3164,
        "url": "https://www.pexels.com/photo/cold-glacier-snow-road-3805681/",
        "photographer": "@rrinna",
        "photographer_url": "https://www.pexels.com/@rrrinna",
        "photographer_id": 824302,
        "avg_color": "#8E96A9",
        "src": {
          "original": "https://images.pexels.com/photos/3805681/pexels-photo-3805681.jpeg",
          "large2x": "https://images.pexels.com/photos/3805681/pexels-photo-3805681.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/3805681/pexels-photo-3805681.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/3805681/pexels-photo-3805681.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/3805681/pexels-photo-3805681.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/3805681/pexels-photo-3805681.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/3805681/pexels-photo-3805681.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/3805681/pexels-photo-3805681.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      },
      {
        "id": 3805689,
        "width": 5777,
        "height": 3688,
        "url": "https://www.pexels.com/photo/cold-snow-wood-man-3805689/",
        "photographer": "@rrinna",
        "photographer_url": "https://www.pexels.com/@rrrinna",
        "photographer_id": 824302,
        "avg_color": "#6C6D70",
        "src": {
          "original": "https://images.pexels.com/photos/3805689/pexels-photo-3805689.jpeg",
          "large2x": "https://images.pexels.com/photos/3805689/pexels-photo-3805689.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/3805689/pexels-photo-3805689.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/3805689/pexels-photo-3805689.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/3805689/pexels-photo-3805689.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/3805689/pexels-photo-3805689.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/3805689/pexels-photo-3805689.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/3805689/pexels-photo-3805689.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      },
      {
        "id": 6036190,
        "width": 3726,
        "height": 4658,
        "url": "https://www.pexels.com/photo/snow-wood-light-dawn-6036190/",
        "photographer": "Oskar  Smethurst",
        "photographer_url": "https://www.pexels.com/@smetovisuals",
        "photographer_id": 8330698,
        "avg_color": "#7D8487",
        "src": {
          "original": "https://images.pexels.com/photos/6036190/pexels-photo-6036190.jpeg",
          "large2x": "https://images.pexels.com/photos/6036190/pexels-photo-6036190.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/6036190/pexels-photo-6036190.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/6036190/pexels-photo-6036190.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/6036190/pexels-photo-6036190.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/6036190/pexels-photo-6036190.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/6036190/pexels-photo-6036190.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/6036190/pexels-photo-6036190.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      },
      {
        "id": 3805687,
        "width": 1932,
        "height": 2955,
        "url": "https://www.pexels.com/photo/cold-snow-road-landscape-3805687/",
        "photographer": "@rrinna",
        "photographer_url": "https://www.pexels.com/@rrrinna",
        "photographer_id": 824302,
        "avg_color": "#8A95B9",
        "src": {
          "original": "https://images.pexels.com/photos/3805687/pexels-photo-3805687.jpeg",
          "large2x": "https://images.pexels.com/photos/3805687/pexels-photo-3805687.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/3805687/pexels-photo-3805687.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/3805687/pexels-photo-3805687.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/3805687/pexels-photo-3805687.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/3805687/pexels-photo-3805687.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/3805687/pexels-photo-3805687.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/3805687/pexels-photo-3805687.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      },
      {
        "id": 3805688,
        "width": 5259,
        "height": 3117,
        "url": "https://www.pexels.com/photo/snow-covered-mountains-3805688/",
        "photographer": "@rrinna",
        "photographer_url": "https://www.pexels.com/@rrrinna",
        "photographer_id": 824302,
        "avg_color": "#718184",
        "src": {
          "original": "https://images.pexels.com/photos/3805688/pexels-photo-3805688.jpeg",
          "large2x": "https://images.pexels.com/photos/3805688/pexels-photo-3805688.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=2\u0026h=650\u0026w=940",
          "large": "https://images.pexels.com/photos/3805688/pexels-photo-3805688.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=650\u0026w=940",
          "medium": "https://images.pexels.com/photos/3805688/pexels-photo-3805688.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=350",
          "small": "https://images.pexels.com/photos/3805688/pexels-photo-3805688.jpeg?auto=compress\u0026cs=tinysrgb\u0026h=130",
          "portrait": "https://images.pexels.com/photos/3805688/pexels-photo-3805688.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=1200\u0026w=800",
          "landscape": "https://images.pexels.com/photos/3805688/pexels-photo-3805688.jpeg?auto=compress\u0026cs=tinysrgb\u0026fit=crop\u0026h=627\u0026w=1200",
          "tiny": "https://images.pexels.com/photos/3805688/pexels-photo-3805688.jpeg?auto=compress\u0026cs=tinysrgb\u0026dpr=1\u0026fit=crop\u0026h=200\u0026w=280"
        },
        "liked": false
      }
    ],
    "total_results": 65,
    "next_page": "https://api.pexels.com/v1/search/?page=2\u0026per_page=10\u0026query=forrest"
  };

  static const Map<String, dynamic> exampleResponse2 = {};
}

class Photos {
  int id;
  int width;
  int height;
  String url;
  String photographer;
  String photographerUrl;
  int photographerId;
  String avgColor;
  Src src;
  bool liked;

  Photos({this.id, this.width, this.height, this.url, this.photographer, this.photographerUrl, this.photographerId, this.avgColor, this.src, this.liked});

  Photos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    width = json['width'];
    height = json['height'];
    url = json['url'];
    photographer = json['photographer'];
    photographerUrl = json['photographer_url'];
    photographerId = json['photographer_id'];
    avgColor = json['avg_color'];
    src = json['src'] != null ? new Src.fromJson(json['src']) : null;
    liked = json['liked'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['width'] = this.width;
    data['height'] = this.height;
    data['url'] = this.url;
    data['photographer'] = this.photographer;
    data['photographer_url'] = this.photographerUrl;
    data['photographer_id'] = this.photographerId;
    data['avg_color'] = this.avgColor;
    if (this.src != null) {
      data['src'] = this.src.toJson();
    }
    data['liked'] = this.liked;
    return data;
  }
}

class Src {
  String original;
  String large2x;
  String large;
  String medium;
  String small;
  String portrait;
  String landscape;
  String tiny;

  Src({this.original, this.large2x, this.large, this.medium, this.small, this.portrait, this.landscape, this.tiny});

  Src.fromJson(Map<String, dynamic> json) {
    original = json['original'];
    large2x = json['large2x'];
    large = json['large'];
    medium = json['medium'];
    small = json['small'];
    portrait = json['portrait'];
    landscape = json['landscape'];
    tiny = json['tiny'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['original'] = this.original;
    data['large2x'] = this.large2x;
    data['large'] = this.large;
    data['medium'] = this.medium;
    data['small'] = this.small;
    data['portrait'] = this.portrait;
    data['landscape'] = this.landscape;
    data['tiny'] = this.tiny;
    return data;
  }
}
