class GetPhotosRequest {
  String query;
  String orientation;
  String size;
  String color;
  String locale;
  int perPage;
  int page;

  GetPhotosRequest({this.query, this.orientation, this.size, this.color, this.locale, this.perPage, this.page});

  GetPhotosRequest.fromJson(Map<String, dynamic> json) {
    query = json['query'];
    orientation = json['orientation'];
    size = json['size'];
    color = json['color'];
    locale = json['locale'];
    perPage = json['per_page'];
    page = json['page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['query'] = this.query;
    data['orientation'] = this.orientation;
    data['size'] = this.size;
    data['color'] = this.color;
    data['locale'] = this.locale;
    data['per_page'] = this.perPage;
    data['page'] = this.page;
    return data;
  }
}
