import 'package:gallery/env.dart';

class ClientBase {
  Map<String, String> header() {
    Map<String, String> header = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "Authorization": Env.pexelsApiKey,
    };
    return header;
  }
}
