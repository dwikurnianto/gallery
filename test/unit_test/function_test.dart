import 'package:flutter_test/flutter_test.dart';
import 'package:gallery/env.dart';
import 'package:gallery/model/photos/GetPhotosRequest.dart';
import 'package:gallery/util/function.dart';

void main() {
  test('Test util function [getQueryString]', () async {
    /// Create dummy request
    GetPhotosRequest _request = new GetPhotosRequest(query: "forest", page: 1, perPage: 10);

    /// Get base url from env and combine with end point
    String url = "${Env.baseApiPexels}/search";

    /// Add url to query string result
    url += getQueryString(_request.toJson());

    /// Test result validate
    expect(url, "https://api.pexels.com/v1/search?query=forest&per_page=10&page=1");
  });
}
