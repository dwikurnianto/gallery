import 'dart:async';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:gallery/model/photos/GetPhotosResponse.dart';
import 'package:gallery/model/photos/GetPhotosRequest.dart';
import 'package:gallery/repository/photo_client.dart';
import 'package:mockito/mockito.dart';

class MockPhotoClient extends Mock implements PhotoClient {}

void main() {
  MockPhotoClient _mockPhotoClient = new MockPhotoClient();
  group('Get photo from pexels', () {
    setUp(() {});
    tearDown(() {});

    test('Test successful asyncrounus', () async {
      /// Http request
      GetPhotosRequest _request = new GetPhotosRequest(query: "forest", page: 1, perPage: 10);

      /// Mock async behaviour
      when(_mockPhotoClient.getPhotos(_request)).thenAnswer((realInvocation) => Future.value(GetPhotosResponse.fromJson(GetPhotosResponse.exampleResponse)));

      /// Test case
      Map<String, dynamic> _result = await _mockPhotoClient.getPhotos(_request).then((value) => value.toJson());
      Map<String, dynamic> _expected = GetPhotosResponse.fromJson(GetPhotosResponse.exampleResponse).toJson();

      /// Test result validate
      expect(_result, _expected);
    });

    test('Test unsuccessful asyncrounus (no internet)', () async {
      /// Http request
      GetPhotosRequest _request = new GetPhotosRequest(query: "forest", page: 1, perPage: 10);

      /// Mock async behaviour
      when(_mockPhotoClient.getPhotos(_request)).thenThrow(SocketException("No internet connection"));

      /// Test result validate
      expect(() => _mockPhotoClient.getPhotos(_request), throwsException);
    });
    test('Test unsuccessful asyncrounus (timeout)', () async {
      /// Http request
      GetPhotosRequest _request = new GetPhotosRequest(query: "forest", page: 1, perPage: 10);

      /// Mock async behaviour
      when(_mockPhotoClient.getPhotos(_request)).thenThrow(TimeoutException("Server hangup"));

      /// Test result validate
      expect(() => _mockPhotoClient.getPhotos(_request), throwsException);
    });
  });
}
