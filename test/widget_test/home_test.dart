import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gallery/env.dart';
import 'package:gallery/model/photos/GetPhotosRequest.dart';
import 'package:gallery/model/photos/GetPhotosResponse.dart';
import 'package:gallery/persentation/screen/home.dart';
import 'package:gallery/repository/photo_client.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';

import '../config/test_material_widget.dart';

class MockPhotoClient extends Mock implements PhotoClient {}

class MockHttpClient extends Mock implements Client {}

// Generate a MockClient using the Mockito package.
// Create new instances of this class in each test.
void main() {
  group("Home page widget test", () {
    testWidgets('Test http client (mockito)', (WidgetTester tester) async {
      MockHttpClient _mockHttpClient = new MockHttpClient();
      MockPhotoClient _mockPhotoClient = new MockPhotoClient();

      /// Http request
      GetPhotosRequest _request = new GetPhotosRequest(query: "forest", page: 1, perPage: 10);

      /// Mock async behaviour
      when(_mockHttpClient.get(any)).thenAnswer((_) => Future.value(Response(jsonEncode(GetPhotosResponse.exampleResponse), 200)));
      when(_mockPhotoClient.getPhotos(_request)).thenAnswer((_) => Future.value(new GetPhotosResponse.fromJson(GetPhotosResponse.exampleResponse)));

      /// Then pump widget with test material widget (MaterialApp) for testing only
      await tester.pumpWidget(TestMaterialWidget(homePage: Home()));

      /// Detect home scaffold first
      expect(find.byKey(Key("HomeScaffold")), findsOneWidget);

      /// Refresh
      await tester.pump();

      /// Detect for loader
      expect(find.byKey(Key("LoaderWidget")), findsOneWidget);

      /// Refresh
      await tester.pump();

      /// Detect if no data
      expect(find.byKey(Key("NoDataWidget")), findsOneWidget);
    });
    testWidgets('Test when there is no data', (WidgetTester tester) async {
      MockHttpClient _mockHttpClient = new MockHttpClient();
      MockPhotoClient _mockPhotoClient = new MockPhotoClient();

      /// Http request
      GetPhotosRequest _request = new GetPhotosRequest(query: "forest", page: 1, perPage: 10);

      /// Mock async behaviour
      when(_mockHttpClient.get(any)).thenAnswer((_) => Future.value(Response(jsonEncode(GetPhotosResponse.exampleResponse), 200)));
      when(_mockPhotoClient.getPhotos(_request)).thenAnswer((_) => Future.value(new GetPhotosResponse.fromJson(GetPhotosResponse.exampleResponse)));

      /// Then pump widget with test material widget (MaterialApp) for testing only
      await tester.pumpWidget(TestMaterialWidget(homePage: Home()));

      /// Detect home scaffold first
      expect(find.byKey(Key("HomeScaffold")), findsOneWidget);

      /// Refresh
      await tester.pump();

      /// Detect for loader
      expect(find.byKey(Key("LoaderWidget")), findsOneWidget);

      /// Refresh
      await tester.pump();

      /// Detect if no data
      expect(find.byKey(Key("NoDataWidget")), findsOneWidget);
    });

    testWidgets('Test when there is no internet connection', (WidgetTester tester) async {
      MockHttpClient _mockHttpClient = new MockHttpClient();
      MockPhotoClient _mockPhotoClient = new MockPhotoClient();

      /// Http request
      GetPhotosRequest _request = new GetPhotosRequest(query: "forest", page: 1, perPage: 10);

      /// Mock async behaviour
      when(_mockHttpClient.get(any)).thenAnswer((_) => Future.value(Response(jsonEncode(GetPhotosResponse.exampleResponse), 200)));
      when(_mockPhotoClient.getPhotos(_request)).thenThrow(SocketException("No internet connection"));

      /// Then pump widget with test material widget (MaterialApp) for testing only
      await tester.pumpWidget(TestMaterialWidget(homePage: Home()));

      /// Detect home scaffold first
      expect(find.byKey(Key("HomeScaffold")), findsOneWidget);

      /// Refresh
      await tester.pump();

      /// Detect if no data
      expect(find.byKey(Key("LoaderWidget")), findsOneWidget);

      /// Refresh
      await tester.pump();

      /// Detect if no data
      expect(find.byKey(Key("NoDataWidget")), findsOneWidget);
    });
  });
}

Map<String, String> header() {
  Map<String, String> header = {
    "Content-type": "application/json",
    "Accept": "application/json",
    "Authorization": Env.pexelsApiKey,
  };
  return header;
}
