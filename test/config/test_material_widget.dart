import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gallery/config/providers.dart';
import 'package:gallery/config/routes.dart';
import 'package:gallery/config/themes.dart';
import 'package:provider/provider.dart';

import 'test_navigator_observer.dart';

class TestMaterialWidget extends StatelessWidget {
  final NavigatorObserver navigatorObserver;
  final Widget homePage;
  TestMaterialWidget({Key key, this.navigatorObserver, this.homePage});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
          currentFocus.focusedChild.unfocus();
        }
      },
      child: MultiProvider(
        providers: providers(),
        child: MaterialApp(
          title: 'Popaket',
          debugShowCheckedModeBanner: false,
          theme: themes(context),
          home: homePage,
          routes: routes(),
          navigatorObservers: [navigatorObserver ?? TestNavigatorObserver()],
          onGenerateRoute: (settings) => generatedRoutes(settings),
        ),
      ),
    );
  }
}
